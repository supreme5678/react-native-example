import React, {Component} from 'react';

const withName = Component => {
  return <Component name={this.context} {...this.props} />;
};

export default withName;
