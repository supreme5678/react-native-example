/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import Stacker from './Stacker';
import NavigationCon from './NaviagationCon';
AppRegistry.registerComponent(appName, () => NavigationCon);
