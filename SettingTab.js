import React from 'react';
import {View, Text, Button} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';
import {TextInput} from 'react-native-gesture-handler';

//yarn add react-native-keyboard-aware-scroll-view
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

const SettingTab = () => {
  return (
    <SettingsStack.Navigator>
      <SettingsStack.Screen name="Settings" component={SettingsScreen} />
      <SettingsStack.Screen name="Me" component={MeScreen} />
    </SettingsStack.Navigator>
  );
};
function MeScreen() {
  return (
    //android:windowSoftInputMode="adjustResize"
    <KeyboardAwareScrollView>
      <View style={{flex: 1, alignItems: 'center'}}>
        <Text>
          Lorem Ipsum es simplemente el texto de relleno de las imprentas y
          archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de
          las industrias desde el año 1500, cuando un impresor (N. del T.
          persona que se dedica a la imprenta) desconocido usó una galería de
          textos y los mezcló de tal manera que logró hacer un libro de textos
          especimen. No sólo sobrevivió 500 años, sino que tambien ingresó como
          texto de relleno en documentos electrónicos, quedando esencialmente
          igual al original. Fue popularizado en los 60s con la creación de las
          hojas "Letraset", las cuales contenian pasajes de Lorem Ipsum, y más
          recientemente con software de autoedición, como por ejemplo Aldus
          PageMaker, el cual incluye versiones de Lorem Ipsum.como por ejemplo
          Aldus PageMaker, el cual incluye versiones de Lorem Ipsum.como por
          ejemplo Aldus PageMaker, el cual incluye versiones de Lorem Ipsum.como
          por ejemplo Aldus PageMaker, el cual incluye versiones de Lorem Ipsum.
          y los mezcló de tal manera que logró hacer un libro de textos
          especimen. No sólo sobrevivió 500 años, sino que tambien ingresó como
          texto de relleno en documentos electrónicos, quedando esencialmente
          igual al original. Fue popularizado en los 60s con la creación de las
          hojas "Letraset", las cuales contenian pasajes de Lorem Ipsum, y más
          recientemente con software de autoedición, como por ejemplo Aldus
          PageMaker, el cual incluye versiones de Lorem
        </Text>
        <TextInput
          style={{backgroundColor: 'gray', width: 300,marginVertical:30}}
          placeholder="keyboard avoiding"></TextInput>
      </View>
    </KeyboardAwareScrollView>
  );
}
function SettingsScreen({navigation}) {
  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <Text>Settings!</Text>
      <Button title="Go to Me" onPress={() => navigation.navigate('Me')} />
    </View>
  );
}

const SettingsStack = createStackNavigator();

export default SettingTab;
