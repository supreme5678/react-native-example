import React, {Component} from 'react';
import {Text, View, StyleSheet} from 'react-native';

export default class MyText extends Component {
  render() {
    return (
      <Text
        style={[
          styles.text,
          this.props.style,
          {fontStyle: this.props.italic ? 'italic' : 'normal'},
        ]}>
        {this.props.children}
      </Text>
    );
  }
}

const styles = StyleSheet.create({
  text: {
    fontSize: 100,
  },
});
