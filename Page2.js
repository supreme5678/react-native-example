import React from 'react';
import {View, Text, Button} from 'react-native';

const Page2 = ({navigation}) => {
  return (
    <View>
      <Text>Page2</Text>
      <Button
        onPress={() => navigation.navigate('Page3')}
        title="touch"></Button>
    </View>
  );
};

export default Page2;
