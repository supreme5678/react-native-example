import React from 'react';
import {View, Text, Button} from 'react-native';
import MyText from './MyText';

const Page1 = ({navigation}) => {
  return (
    <View>
      <Text>Page1</Text>
      <MyText italic style={{color: 'green'}}>Wow</MyText>
      <MyText style={{color: 'green'}}>No</MyText>

      <Button onPress={() => navigation.navigate('Page2')} title="Dont"></Button>
    </View>
  );
};

export default Page1;
