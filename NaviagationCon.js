import React, {useState} from 'react';
import {Button, Text, View, StatusBar, FlatList} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import SettingTab from './SettingTab';
import MyText from './MyText';
import SafeAreaView from 'react-native-safe-area-view';

const numberList = [
  124,
  155,
  248,
  266,
  273,
  290,
  296,
  304,
  329,
  340,
  342,
  369,
  378,
  399,
  403,
  415,
  416,
  417,
  464,
  472,
  490,
  518,
  525,
  560,
  572,
  597,
  598,
  599,
  603,
  651,
  657,
  711,
  721,
  726,
  735,
  763,
  774,
  777,
  786,
  807,
  814,
  855,
  882,
  905,
  908,
  910,
  927,
  939,
  984,
  995,
];

function HomeScreen({navigation}) {
  console.log('kiki');
  return (
    <>
      <StatusBar
        translucent={true}
        backgroundColor={'transparent'}
        barStyle="dark-content"></StatusBar>
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Text
          style={{
            backgroundColor: '#fff',
            elevation: 8,
          }}>
          Shadow
        </Text>
        <MyText italic style={{color: 'green'}}>
          Italic
        </MyText>
        <MyText style={{color: 'red'}}>No</MyText>
        <Button
          title="Go to Details"
          onPress={() => navigation.navigate('Details')}
        />
      </View>
    </>
  );
}

function DetailsScreen() {
  const [isFetching, setFetch] = useState(false);

  const onRefreshing = () => {
    setFetch(true);
    setTimeout(() => {
      setFetch(false);
    }, 3000);
  };

  return (
    <SafeAreaView style={{flex: 1, marginTop: 0}} forceInset={{top: 'always'}}>
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <FlatList
          data={numberList}
          renderItem={({item}) => (
            <View>
              <Text
                style={{
                  width: 350,
                  borderWidth: 1,
                  borderColor: 'red',
                  padding: 20,
                }}>
                {item}
                {console.log(item)}
              </Text>
            </View>
          )}
          keyExtractor={(index, item) => index + '' + item}
          onRefresh={() => {
            onRefreshing();
          }}
          refreshing={isFetching}></FlatList>
      </View>
    </SafeAreaView>
  );
}

function HomeStackScreen() {
  return (
    <HomeStack.Navigator screenOptions={navigationOptions}>
      <HomeStack.Screen name="Home" component={HomeScreen} />
      <HomeStack.Screen name="Details" component={DetailsScreen} />
    </HomeStack.Navigator>
  );
}

const HomeStack = createStackNavigator();
const Tab = createBottomTabNavigator();
const navigationOptions = {
  headerShown: false,
};

export default function NavigationCon() {
  return (
    <NavigationContainer>
      <Tab.Navigator>
        <Tab.Screen name="Home" component={HomeStackScreen} />
        <Tab.Screen name="Settings" component={SettingTab} />
      </Tab.Navigator>
    </NavigationContainer>
  );
}
